const
    { mkdir, writeFile, readFile } = require('fs'),
    { join: joinPath } = require('path'),
    outdent = require('outdent'),
    Parcel = require('parcel-bundler'),
    { description } = require('./package.json'),
    { url } = require('./config.json'),
    mainRustFilePath = joinPath(__dirname, './src-tauri/src/main.rs');
(async () => {
    await new Promise(resolve => mkdir(joinPath(__dirname, './dist'), resolve));
    await new Promise(resolve => writeFile(joinPath(__dirname, './dist/index.html'), outdent `
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <title>${description}</title>
                <script>
                    window.location.replace('${url}');
                </script>
            </head>
        </html>
    `, 'utf8', resolve));
    const mainRustFileContent = await new Promise(resolve => readFile(mainRustFilePath, 'utf8', (_, data) => resolve(data)));
    await new Promise(resolve => writeFile(
        mainRustFilePath,
        mainRustFileContent.replace(
            /^(\s+tauri::Builder::default\(\)).*$/m,
            '$1.on_page_load(|window, _| { let _ = window.eval(include_str!("../../dist/index.js")); })'
        ),
        'utf8',
        resolve
    ));
    await new Parcel(joinPath(__dirname, './src/index.js')).bundle();
    process.exit();
})();