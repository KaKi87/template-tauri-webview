const
    { spawn } = require('child_process'),
    {
        name,
        description
    } = require('./package.json');
spawn('./node_modules/.bin/tauri', [
    'init',
    '--ci',
    `--app-name=${name}`,
    '--dist-dir=../dist',
    '--dev-path=../dist',
    `--window-title=${description}`
]);